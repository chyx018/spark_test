package com.eighteen.app

import java.text.SimpleDateFormat
import java.util.Date

import com.alibaba.fastjson.{JSON, JSONObject}
import com.eighteen.utils.MyKafkaUtil
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.{Seconds, StreamingContext}


object DauApp {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .config("spark.sql.parquet.writeLegacyFormat", "true")
      .config("spark.sql.sources.partitionOverwriteMode", "dynamic") //覆盖分区
      .enableHiveSupport()
      .master("local[4]")
      .getOrCreate()
    val ssc = new StreamingContext(spark.sparkContext, Seconds(10))
//===============消费Kafka数据===================
    //val groupId = "gmall_dau_bak"
    val topic = "sparktest"
    val recordDstream: InputDStream[ConsumerRecord[String, String]] = MyKafkaUtil.getKafkaStream(topic,ssc)

    val schema = StructType(
      List(
        StructField("union_id", StringType, true),
        StructField("coid", IntegerType, true),
        StructField("ncoid", IntegerType, true),
        StructField("channel_id", IntegerType, true),
        StructField("ver_name", StringType, true),
        StructField("content_id", StringType, true),
        StructField("content_source", StringType, true),
        StructField("content_source_id", IntegerType, true),
        StructField("create_time", StringType, true),
        StructField("play_duration", IntegerType, true),
        StructField("type", IntegerType, true),
        StructField("type_name", StringType, true),
        StructField("dt", StringType, true)
      )
    )
     recordDstream.foreachRDD(rdd=> {
      val trdd = rdd.map(_.value()).map ({ x =>
        val jsonObj = JSON.parseObject(x)
        //获取时间
        val time = jsonObj.getString("eventTime")
        //转换日期
        val date1 = new Date()
        date1.setTime(time.toLong)
        val date = new SimpleDateFormat("yyyy-MM-dd").format(date1)
        jsonObj.put("dt", date)
        jsonObj
      })
        .map(x => Row(
          x.getString("union_id"),
          x.getInteger("coid"),
          x.getInteger("ncoid"),
          x.getInteger("channelId"),
          x.getString("verName"),
          x.getString("contentId"),
          x.getString("contentSource"),
          x.getInteger("contentSourceId"),
          x.getString("createTime"),
          x.getInteger("playDuration"),
          x.getInteger("type"),
          x.getString("typeName"),
          x.getString("dt")
        ))
       //将数据与元数据拼凑
       val rddDF = spark.createDataFrame(trdd,schema)
       rddDF.show()
       if (rddDF.count()>0) {
        //写入hive
         rddDF.createOrReplaceTempView("tmp_table")
         spark.sql("select * from tmp_table").write.mode("append").saveAsTable("dwd_label_product_content_event_di")
       }
    })

    ssc.start()
    ssc.awaitTermination()
  }
}
