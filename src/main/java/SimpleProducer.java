import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
 

public class SimpleProducer {
    public static void main(String[] args) throws Exception {
 
        // Assign topicName to string variable
        String topicName = "sparktest";
 
        // create instance for properties to access producer configs
        Properties props = new Properties();
 
        // Assign localhost id, 参考http://kafka.apache.org/documentation/#producerapi
        props.put("bootstrap.servers", "192.168.1.102:9092");
 
        // Set acknowledgements for producer requests.
        props.put("acks", "all");
 
        // If the request fails, the producer can automatically retry,
        props.put("retries", 0);
 
        // Specify buffer size in config
        props.put("batch.size", 16384);
 
        // Reduce the no of requests less than 0
        props.put("linger.ms", 1);
 
        // The buffer.memory controls the total amount of memory available to the
        // producer for buffering.
        props.put("buffer.memory", 33554432);
 
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
 
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
 
        Producer<String, String> producer = new KafkaProducer<String, String>(props);
 
 /*
        int i = 0 ;
        while (i < 100) {
            String tempString = UUID.randomUUID().toString();
            System.out.println("----------"+tempString);
            producer.send(new ProducerRecord<String, String>(topicName, tempString));
            Thread.sleep(1000);
            i++ ;
        }

  */
        JSONObject obj = new JSONObject();
        obj.put("union_id","16161");
        obj.put("coid",2616);
        obj.put("ncoid",100);
        obj.put("channelId",20);
        obj.put("verName","1.5");
        obj.put("contentId","191");
        obj.put("contentSource","161");
        obj.put("contentSourceId",26);
        obj.put("createTime","1637682470000");
        obj.put("playDuration",100);
        obj.put("type",26);
        obj.put("typeName","16161");
        obj.put("eventTime","1570650412089");

        producer.send(new ProducerRecord(topicName, obj.toJSONString()));


        System.out.println("Message sent successfully");
        producer.close();
    }
}
 
